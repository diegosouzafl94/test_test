class Carro

  def velocidade_maxima
    250
  end

  attr_accessor :marca, :modelo

  def descricao
    "Marca: #{@marca} e Modelo: #{@modelo}"
  end

end

carro = Carro.new
carro.marca = "Ford"
carro.modelo = "Focus"
puts "Marca: " + carro.marca
puts "Modelo: " + carro.modelo
puts "Descricao: " + carro.descricao
